package dev.tacon.jakartaee.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;

import dev.tacon.jakartaee.dto.reference.DtoIntRef;

public class DtoUtilsTest {

	@Test
	public void testRefs() {
		final MyDto dto1 = new MyDto(1);
		final MyDto dto2 = new MyDto(2);

		final Collection<MyDto> dtos = List.of(dto1, dto2);
		final Set<DtoIntRef<MyDto>> refs = DtoUtils.refs(dtos);

		assertNotNull(refs);
		assertEquals(2, refs.size());
		assertNull(DtoUtils.refs(null));
	}

	@Test
	public void testRefIds() {
		final DtoIntRef<MyDto> ref1 = new DtoIntRef<>(1);
		final DtoIntRef<MyDto> ref2 = new DtoIntRef<>(2);

		final Collection<DtoIntRef<MyDto>> refs = List.of(ref1, ref2);
		final Set<Integer> refIds = DtoUtils.refIds(refs);

		assertNotNull(refIds);
		assertEquals(2, refIds.size());
		assertNull(DtoUtils.refIds(null));
	}

	@Test
	public void testIds() {
		final MyDto dto1 = new MyDto(1);
		final MyDto dto2 = new MyDto(2);

		final Collection<MyDto> dtos = List.of(dto1, dto2);
		final Set<Integer> ids = DtoUtils.ids(dtos);

		assertNotNull(ids);
		assertEquals(2, ids.size());
		assertNull(DtoUtils.ids(null));
	}

	@Test
	public void testRef() {
		final MyDto dto = new MyDto(1);

		assertEquals(dto.getRef(), DtoUtils.ref(dto));
		assertNull(DtoUtils.ref((MyDto) null));

		assertEquals(new DtoIntRef<>(1), DtoUtils.ref(Integer.valueOf(1)));
		assertNull(DtoUtils.ref((Integer) null));
	}

	@Test
	public void testId() {
		final MyDto dto = new MyDto(1);

		assertEquals(dto.getId(), DtoUtils.id(dto));
		assertNull(DtoUtils.id((MyDto) null));
	}
}