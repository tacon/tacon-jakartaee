package dev.tacon.jakartaee.dto.exception;

/**
 * Custom exception class that represents generic issues during operations on DTOs.
 * The class contains an additional {@code errorCode} to further categorize or identify the exception.
 */
public abstract class ManagerException extends Exception {

	private static final long serialVersionUID = 982944505921493130L;

	private final String errorCode;

	/**
	 * Constructs a new {@code ManagerException} with the specified error code.
	 *
	 * @param errorCode the code representing the specific error.
	 */
	public ManagerException(final String errorCode) {
		super();
		this.errorCode = errorCode;
	}

	/**
	 * Constructs a new {@code ManagerException} with the specified error code, message and cause.
	 *
	 * @param errorCode the code representing the specific error.
	 * @param message the detail message.
	 * @param cause the cause of the exception.
	 */
	public ManagerException(final String errorCode, final String message, final Throwable cause) {
		super(message, cause);
		this.errorCode = errorCode;
	}

	/**
	 * Constructs a new {@code ManagerException} with the specified error code and message.
	 *
	 * @param errorCode the code representing the specific error.
	 * @param message the detail message.
	 */
	public ManagerException(final String errorCode, final String message) {
		super(message);
		this.errorCode = errorCode;
	}

	/**
	 * Constructs a new {@code ManagerException} with the specified error code and cause.
	 *
	 * @param errorCode the code representing the specific error.
	 * @param cause the cause of the exception.
	 */
	public ManagerException(final String errorCode, final Throwable cause) {
		super(cause);
		this.errorCode = errorCode;
	}

	/**
	 * Retrieves the error code associated with this exception.
	 *
	 * @return the error code.
	 */
	public String getErrorCode() {
		return this.errorCode;
	}
}
