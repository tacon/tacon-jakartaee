package dev.tacon.jakartaee.dto;

import java.io.Serializable;
import java.lang.reflect.Constructor;

import dev.tacon.annotations.NonNull;

/**
 * This interface represents a Data Transfer Object (DTO) that is Serializable.
 * DTOs are commonly used for transmitting data across different layers of an application,
 * or between different applications. Within a Jakarta EE environment, this interface
 * can be utilized to standardize the transfer of data between the presentation layer
 * and the business logic layer.
 * <p>
 * Classes implementing this interface are required to have a no-argument constructor.
 * </p>
 *
 * @see java.io.Serializable
 */
public interface Dto extends Serializable {

	/**
	 * Factory method to create a new instance of the DTO.
	 * This method assumes that the provided class type has a no-argument constructor,
	 * which is a requirement in Jakarta EE for managed beans and some other component types.
	 *
	 * @param <D> type of the DTO to create.
	 * @param dtoClass class of the DTO to create.
	 * @return a new instance of the DTO class.
	 */
	@SuppressWarnings("unchecked")
	static @NonNull <D extends Dto> D of(final @NonNull Class<D> dtoClass) {
		try {
			final Constructor<?> constr = dtoClass.getConstructor();
			return (D) constr.newInstance();
		} catch (final ReflectiveOperationException roex) {
			throw new RuntimeException("Cannot find an empty constructor for " + dtoClass, roex);
		}
	}
}
