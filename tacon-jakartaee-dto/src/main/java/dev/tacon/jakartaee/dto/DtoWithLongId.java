package dev.tacon.jakartaee.dto;

import dev.tacon.jakartaee.dto.reference.DtoLongRef;

/**
 * Represents a data transfer object (DTO) with an associated long ID and reference.
 *
 * @param <D> the type of DTO.
 */
public class DtoWithLongId<D extends DtoWithLongId<D>> extends DtoWithId<D, Long, DtoLongRef<D>> {

	private static final long serialVersionUID = 951210959671517022L;

	/**
	 * Protected required default constructor.
	 * Initializes with a {@code null} reference.
	 */
	protected DtoWithLongId() {
		super(null);
	}

	/**
	 * Constructor to initialize with a primitive long ID.
	 *
	 * @param id the long ID to set.
	 */
	protected DtoWithLongId(final long id) {
		this(Long.valueOf(id));
	}

	/**
	 * Constructor to initialize with an Long ID.
	 *
	 * @param id the Long ID to set, not null.
	 */
	protected DtoWithLongId(final Long id) {
		super(new DtoLongRef<D>(id));
	}

	/**
	 * Constructor to initialize with a specific reference.
	 *
	 * @param ref the reference to set, or {@code null}.
	 */
	protected DtoWithLongId(final DtoLongRef<D> ref) {
		super(ref);
	}
}
