package dev.tacon.jakartaee.dto;

import java.util.UUID;

import dev.tacon.jakartaee.dto.reference.DtoUuidRef;

/**
 * Represents a data transfer object (DTO) with an associated UUID and reference.
 *
 * @param <D> the type of DTO.
 */
public abstract class DtoWithUuid<D extends DtoWithUuid<D>> extends DtoWithId<D, UUID, DtoUuidRef<D>> {

	private static final long serialVersionUID = -555695435670136550L;

	/**
	 * Protected required default constructor.
	 * Initializes with a {@code null} reference.
	 */
	protected DtoWithUuid() {
		super(null);
	}

	/**
	 * Constructor to initialize with a UUID.
	 *
	 * @param id the UUID to set, not null.
	 */
	protected DtoWithUuid(final UUID id) {
		super(new DtoUuidRef<D>(id));
	}

	/**
	 * Constructor to initialize with a specific reference.
	 *
	 * @param ref the reference to set, or {@code null}.
	 */
	protected DtoWithUuid(final DtoUuidRef<D> ref) {
		super(ref);
	}
}
