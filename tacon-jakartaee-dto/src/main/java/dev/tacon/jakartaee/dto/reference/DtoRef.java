package dev.tacon.jakartaee.dto.reference;

import java.io.Serializable;
import java.util.Objects;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee.dto.Dto;

/**
 * Represents a reference to a data transfer object (DTO) with an associated identifier (ID).
 * This class provides a way to refer to DTOs without needing the entire object.
 *
 * @param <D> the type of the DTO to which this reference points.
 * @param <I> the type of the ID associated with the DTO.
 */
public class DtoRef<D extends Dto, I> implements Serializable {

	private static final long serialVersionUID = 2538378520796246528L;

	private I id;

	/**
	 * Protected required default constructor.
	 * Only for internal usage, not public!
	 */
	protected DtoRef() {}

	/**
	 * Constructor that takes an ID for the DTO.
	 *
	 * @param id the identifier of the DTO. Must not be {@code null}.
	 * @throws NullPointerException if the provided ID is {@code null}.
	 */
	public DtoRef(final @NonNull I id) {
		this.id = Objects.requireNonNull(id, "ID cannot be null in a reference");
	}

	/**
	 * Gets the ID associated with the DTO.
	 *
	 * @return the ID associated with the DTO.
	 */
	public I getId() {
		return this.id;
	}

	@Override
	public String toString() {
		return this.id.toString();
	}

	@Override
	public int hashCode() {
		return this.hashCodeId();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		return this.equalsId(((DtoRef<?, ?>) obj).id);
	}

	protected int hashCodeId() {
		return this.id.hashCode();
	}

	/**
	 * Checks the reference equality based on the ID.
	 *
	 * @param other the other ID to be compared.
	 * @return {@code true} if IDs are equal, {@code false} otherwise.
	 */
	protected boolean equalsId(final Object other) {
		return this.id.equals(other);
	}
}
