package dev.tacon.jakartaee.dto;

import dev.tacon.jakartaee.dto.reference.DtoStringRef;

/**
 * Represents a data transfer object (DTO) with an associated string ID and reference.
 *
 * @param <D> the type of DTO.
 */
public class DtoWithStringId<D extends DtoWithStringId<D>> extends DtoWithId<D, String, DtoStringRef<D>> {

	private static final long serialVersionUID = 951210959671517022L;

	/**
	 * Protected required default constructor.
	 * Initializes with a {@code null} reference.
	 */
	protected DtoWithStringId() {
		super(null);
	}

	/**
	 * Constructor to initialize with a String ID.
	 *
	 * @param id the String ID to set, not null.
	 */
	protected DtoWithStringId(final String id) {
		super(new DtoStringRef<D>(id));
	}

	/**
	 * Constructor to initialize with a specific reference.
	 *
	 * @param ref the reference to set, or {@code null}.
	 */
	protected DtoWithStringId(final DtoStringRef<D> ref) {
		super(ref);
	}
}
