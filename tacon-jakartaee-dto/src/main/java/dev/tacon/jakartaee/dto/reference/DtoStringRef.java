package dev.tacon.jakartaee.dto.reference;

import dev.tacon.jakartaee.dto.DtoWithStringId;

/**
 * Represents a reference to a data transfer object (DTO) with an associated string.
 *
 * @param <D> the type of DTO.
 */
public class DtoStringRef<D extends DtoWithStringId<D>> extends DtoRef<D, String> {

	private static final long serialVersionUID = -3652131357431256774L;

	/**
	 * Protected required default constructor.
	 * Only for internal usage, not public!
	 */
	protected DtoStringRef() {
		super();
	}

	/**
	 * Constructor that takes a {@code String} ID for the DTO.
	 *
	 * @param id the identifier of the DTO. Must not be {@code null}.
	 * @throws NullPointerException if the provided ID is {@code null}.
	 */
	public DtoStringRef(final String id) {
		super(id);
	}
}
