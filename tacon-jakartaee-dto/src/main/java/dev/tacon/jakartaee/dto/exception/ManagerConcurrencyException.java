package dev.tacon.jakartaee.dto.exception;

import java.io.Serializable;

/**
 * Exception representing a concurrency issue in a Jakarta EE environment.
 * Typically thrown when there is a mismatch between the expected and current version of a managed object.
 */
public class ManagerConcurrencyException extends RuntimeException {

	private static final long serialVersionUID = -722273837586030163L;
	private final Serializable currentVersion;
	private final Serializable expectedVersion;

	/**
	 * Constructs a new {@code ManagerConcurrencyException} with the specified detail message,
	 * current version, and expected version.
	 *
	 * @param message the detail message explaining the reason for the exception.
	 * @param currentVersion the current version of the object that caused the exception.
	 * @param expectedVersion the expected version of the object.
	 */
	public ManagerConcurrencyException(final String message, final Serializable currentVersion, final Serializable expectedVersion) {
		super(message);
		this.currentVersion = currentVersion;
		this.expectedVersion = expectedVersion;
	}

	/**
	 * Retrieves the current version of the object that caused the exception.
	 *
	 * @return the current version. May be {@code null} if the version is not applicable.
	 */
	public Serializable getCurrentVersion() {
		return this.currentVersion;
	}

	/**
	 * Retrieves the expected version of the object that was supposed to be managed.
	 *
	 * @return the expected version. May be {@code null} if the version is not applicable.
	 */
	public Serializable getExpectedVersion() {
		return this.expectedVersion;
	}
}
