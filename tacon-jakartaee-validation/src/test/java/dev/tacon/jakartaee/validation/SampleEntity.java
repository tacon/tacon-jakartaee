package dev.tacon.jakartaee.validation;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class SampleEntity {

	@TemporalRange(back = 5, forward = 5, unit = ChronoUnit.DAYS)
	private final LocalDate sampleDate;

	public SampleEntity(final LocalDate sampleDate) {
		this.sampleDate = sampleDate;
	}
}