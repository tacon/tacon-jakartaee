package dev.tacon.jakartaee.validation;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;

public class TemporalRangeValidatorForOffsetDateTime extends TemporalRangeValidator<OffsetDateTime, OffsetDateTime> {

	@Override
	protected OffsetDateTime convert(final OffsetDateTime value) {
		return value;
	}

	@Override
	protected OffsetDateTime getCurrent(final ChronoUnit unit) {
		return OffsetDateTime.now();
	}

	@Override
	protected OffsetDateTime calculateMin(final OffsetDateTime current, final int units, final ChronoUnit unit) {
		return current.minus(units, unit);
	}

	@Override
	protected OffsetDateTime calculateMax(final OffsetDateTime current, final int units, final ChronoUnit unit) {
		return current.plus(units, unit);
	}
}
