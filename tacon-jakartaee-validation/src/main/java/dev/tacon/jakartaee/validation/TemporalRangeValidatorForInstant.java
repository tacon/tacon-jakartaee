package dev.tacon.jakartaee.validation;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class TemporalRangeValidatorForInstant extends TemporalRangeValidator<Instant, Instant> {

	@Override
	protected Instant convert(final Instant value) {
		return value;
	}

	@Override
	protected Instant getCurrent(final ChronoUnit unit) {
		return Instant.now();
	}

	@Override
	protected Instant calculateMin(final Instant current, final int units, final ChronoUnit unit) {
		return current.minus(units, unit);
	}

	@Override
	protected Instant calculateMax(final Instant current, final int units, final ChronoUnit unit) {
		return current.plus(units, unit);
	}
}
