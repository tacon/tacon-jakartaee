# tacon-jakartaee


```mermaid
flowchart TD
    subgraph Validation
        direction RL
        jakarta.validation
    end

    subgraph Persistence
        direction RL
        AbstractJpaEntity
        EntityUtils
    end
    
    subgraph Query
        direction RL
        jakarta.persistence
        IQuery...
        IExpression...
    end

    subgraph EjbInterfaces
        direction RL
        jakarta.Ejb
        EjbManagerException
        EjbValidationManagerException
    end

    subgraph Ejb
        direction RL
        jakarta.Ejb
        Manager
    end

Ejb --> EjbInterfaces
Ejb --> Persistence
Ejb --> Validation
Ejb --> Query
```
