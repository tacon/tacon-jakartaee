package dev.tacon.jakartaee.query;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Supplier;

import dev.tacon.jakartaee.query.criteria.IExpression;
import dev.tacon.jakartaee.query.criteria.IFrom;
import dev.tacon.jakartaee.query.criteria.IJoin;
import dev.tacon.jakartaee.query.criteria.IPredicate;
import dev.tacon.jakartaee.query.criteria.IRoot;
import jakarta.persistence.criteria.AbstractQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.From;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Predicate;

@SuppressWarnings("hiding")
class Subquery<F, R> implements ISubquery<F, R> {

	private final Class<R> resultClass;
	private final IExpression<R> selection;
	private boolean distinct;
	private Map<IFrom<?, ?>, From<?, ?>> froms;
	private Map<String, From<?, ?>> fromsById;
	private IRoot<F> mainRoot;
	private List<IPredicate> predicates;
	private List<IExpression<?>> groups;
	private List<IPredicate> havingPredicates;

	private boolean invalidated;

	Subquery(final Class<R> type) {
		this(type, null, null);
	}

	Subquery(final Class<R> type, final Subquery<F, ?> query, final IExpression<R> selection) {
		this.resultClass = type;
		this.selection = selection;
		if (query != null) {
			this.distinct = query.distinct;
			this.froms = query.froms;
			this.fromsById = query.fromsById;
			this.mainRoot = query.mainRoot;
			this.predicates = query.predicates;
			this.groups = query.groups;
			this.havingPredicates = query.havingPredicates;
			query.invalidate();
		} else {
			this.fromsById = new HashMap<>();
			this.froms = new IdentityHashMap<>();
		}
	}

	private void invalidate() {
		this.invalidated = true;
		this.froms = null;
		this.fromsById = null;
		// this.subqueries = null;
		this.predicates = null;
		this.groups = null;
		this.havingPredicates = null;
		this.mainRoot = null;
	}

	@Override
	public ISubquery<F, R> distinct() {
		this.distinct = true;
		return this;
	}

	@Override
	public <T> ISubquery<F, T> select(final Class<T> resultClass, final IExpression<T> selection) {
		return new Subquery<>(resultClass, this, selection);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> ISubquery<T, R> from(final IRoot<T> root, final IFrom<?, ?>... froms) {
		final Subquery<T, R> query = (Subquery<T, R>) this;
		query.froms.clear();
		query.fromsById.clear();
		query.mainRoot = requireNonNull(root);
		query.addFrom(root);
		for (final IFrom<?, ?> from : froms) {
			query.addFrom(from);
		}
		return query;
	}

	@Override
	public ISubquery<F, R> where(final IPredicate... predicates) {
		Collections.addAll(this.getPredicates(), predicates);
		return this;
	}

	@Override
	public <V> ISubquery<F, R> eq(final IExpression<V> expr, final V value) {
		this.getPredicates().add(Expressions.equalOrNull(expr, value));
		return this;
	}

	@Override
	public ISubquery<F, R> groupBy(final IExpression<?>... expressions) {
		if (this.groups == null) {
			this.groups = new ArrayList<>();
		}
		Collections.addAll(this.groups, expressions);
		return this;
	}

	@Override
	public ISubquery<F, R> having(final IPredicate... predicates) {
		if (this.havingPredicates == null) {
			this.havingPredicates = new ArrayList<>();
		}
		Collections.addAll(this.havingPredicates, predicates);
		return this;
	}

	@Override
	public jakarta.persistence.criteria.Subquery<R> create(final IQueryDataHolder data) {
		if (this.invalidated) {
			throw new IllegalStateException("Method select has been called, this subquery class instance should not be used");
		}
		final jakarta.persistence.criteria.Subquery<R> sq = data.getCurrentQuery().subquery(this.resultClass);
		// this.prepareFrom(criteriaQuery);
		// this.prepareSubqueries(criteriaQuery);
		// this.prepareSelect(criteriaQuery);
		// this.prepareWhere(criteriaQuery);
		// this.prepareOrder(criteriaQuery);
		// this.prepareGroup(criteriaQuery);
		// this.prepareHaving(criteriaQuery);
		final SubqueryDataHolder subData = new SubqueryDataHolder(data.getCriteriaBuilder(), sq, data);
		for (final Entry<IFrom<?, ?>, From<?, ?>> entry : this.froms.entrySet()) {
			entry.setValue(entry.getKey().create(subData, sq));
		}
		if (this.selection != null) {
			sq.select(this.selection.resolve(subData));
		}
		if (this.predicates != null) {
			sq.where(this.predicates.stream().map(p -> p.resolve(subData)).toArray(Predicate[]::new));
		}
		if (this.groups != null) {
			sq.groupBy(this.groups.stream().map(g -> g.resolve(subData)).collect(toList()));
		}
		if (this.havingPredicates != null) {
			sq.having(this.havingPredicates.stream().map(p -> p.resolve(subData)).toArray(Predicate[]::new));
		}
		return sq;
	}

	@Override
	public IExpression<R> getSelection() {
		return data -> this.resolve(data).getSelection();
	}

	private void addFrom(final IFrom<?, ?> from) {
		final String identifier = from.getIdentifier();
		if (identifier != null) {
			if (this.fromsById.containsKey(identifier)) {
				throw new IllegalStateException("Duplicate identifier " + identifier);
			}
		}
		this.froms.put(from, null);
	}

	private List<IPredicate> getPredicates() {
		if (this.predicates == null) {
			this.predicates = new ArrayList<>();
		}
		return this.predicates;
	}

	private <S, T> void registerJoin(final IJoin<S, T> iJoin, final Join<S, T> join) {
		final String identifier = iJoin.getIdentifier();
		if (identifier != null) {
			final From<?, ?> existingJoin = this.fromsById.get(identifier);
			if (existingJoin != null) {
				if (join != existingJoin) {
					throw new IllegalStateException("Duplicate identifier: " + identifier);
				}
				return;
			}
			this.fromsById.put(identifier, join);
		}
		this.froms.put(iJoin, join);
	}

	@SuppressWarnings("unchecked")
	private <S, T> From<S, T> getFrom(final IFrom<S, T> from) {
		final String identifier = from.getIdentifier();
		if (identifier != null) {
			return (From<S, T>) this.fromsById.get(identifier);
		}
		return (From<S, T>) this.froms.get(from);
	}

	private IRoot<?> getMainRoot() {
		return this.mainRoot;
	}

	private class SubqueryDataHolder implements IQueryDataHolder {

		private final CriteriaBuilder criteriaBuilder;
		private final jakarta.persistence.criteria.Subquery<?> currentQuery;
		private final IQueryDataHolder parentQuery;

		SubqueryDataHolder(final CriteriaBuilder criteriaBuilder, final jakarta.persistence.criteria.Subquery<?> currentQuery, final IQueryDataHolder parentQuery) {
			this.criteriaBuilder = criteriaBuilder;
			this.currentQuery = currentQuery;
			this.parentQuery = parentQuery;
		}

		@Override
		public CriteriaBuilder getCriteriaBuilder() {
			return this.criteriaBuilder;
		}

		@Override
		public AbstractQuery<?> getCurrentQuery() {
			return this.currentQuery;
		}

		@Override
		public <S, T> void registerJoin(final IJoin<S, T> iJoin, final Join<S, T> join) {
			Subquery.this.registerJoin(iJoin, join);
		}

		@Override
		public <S, T> From<S, T> getFrom(final IFrom<S, T> from) {
			final From<S, T> subqueryFrom = Subquery.this.getFrom(from);
			return subqueryFrom == null ? this.parentQuery.getFrom(from) : subqueryFrom;
		}

		@Override
		public <R> jakarta.persistence.criteria.Subquery<R> getSubquery(final ISubquery<?, R> subquery, final Supplier<jakarta.persistence.criteria.Subquery<R>> supplier) {
			return this.parentQuery.getSubquery(subquery, supplier);
		}

		@Override
		public IRoot<?> getMainRoot() {
			return Subquery.this.getMainRoot();
		}
	}
}
