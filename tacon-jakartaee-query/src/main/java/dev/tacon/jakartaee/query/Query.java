package dev.tacon.jakartaee.query;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Supplier;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee.query.ITupleQuery.TupleMapper;
import dev.tacon.jakartaee.query.criteria.IExpression;
import dev.tacon.jakartaee.query.criteria.IFrom;
import dev.tacon.jakartaee.query.criteria.IJoin;
import dev.tacon.jakartaee.query.criteria.IOrder;
import dev.tacon.jakartaee.query.criteria.IPredicate;
import dev.tacon.jakartaee.query.criteria.IRoot;
import dev.tacon.jakartaee.query.criteria.ISelection;
import jakarta.persistence.EntityManager;
import jakarta.persistence.FlushModeType;
import jakarta.persistence.LockModeType;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.AbstractQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.From;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Selection;
import jakarta.persistence.criteria.Subquery;

@SuppressWarnings({ "unchecked", "hiding" })
abstract class Query<Q, F, R> implements IQuery<Q, F>, IQueryDataHolder {

	protected final EntityManager entityManager;
	protected final CriteriaBuilder criteriaBuilder;
	private AbstractQuery<?> currentQuery;

	private Map<IFrom<?, ?>, From<?, ?>> froms;
	private Map<String, From<?, ?>> fromsById;
	private Map<ISubquery<?, ?>, Subquery<?>> subqueries;
	private List<IPredicate> predicates;
	private List<IOrder> orders;
	private List<IExpression<?>> groups;
	private List<IPredicate> havingPredicates;
	private boolean distinct;
	private LockModeType lockMode;
	private FlushModeType flushMode;
	private int limit = -1;
	private int offset = -1;
	private Map<String, Object> hints;

	private IRoot<F> mainRoot;

	private boolean invalidated;

	Query(final EntityManager entityManager) {
		this.entityManager = entityManager;
		this.criteriaBuilder = entityManager.getCriteriaBuilder();
		this.froms = new IdentityHashMap<>();
		this.fromsById = new HashMap<>();
	}

	protected Query(final Query<?, F, ?> query) {
		this.entityManager = query.entityManager;
		this.criteriaBuilder = query.criteriaBuilder;
		this.froms = query.froms;
		this.fromsById = query.fromsById;
		this.subqueries = query.subqueries;
		this.predicates = query.predicates;
		this.orders = query.orders;
		this.groups = query.groups;
		this.havingPredicates = query.havingPredicates;
		this.distinct = query.distinct;
		this.lockMode = query.lockMode;
		this.flushMode = query.flushMode;
		this.limit = query.limit;
		this.offset = query.offset;
		this.hints = query.hints;
		this.mainRoot = query.mainRoot;
		query.invalidate();
	}

	protected void invalidate() {
		this.invalidated = true;
		this.froms = null;
		this.fromsById = null;
		this.subqueries = null;
		this.predicates = null;
		this.orders = null;
		this.groups = null;
		this.havingPredicates = null;
		this.hints = null;
		this.mainRoot = null;
	}

	protected void invalidateBeforeQuery() {
		this.invalidate();
	}

	@Override
	public <T> ISingleQuery<F, T> select(final Class<T> resultClass, final ISelection<T> selection) {
		return new SingleQuery<>(this, resultClass, selection);
	}

	@Override
	public <T> ISingleQuery<F, T> select(final Class<T> resultClass, final ISelection<?>... selections) {
		return new SingleQuery<>(this, resultClass, data -> compoundSelection(data, resultClass, selections));
	}

	@Override
	public ITupleQuery<F> select(final ISelection<?>... selections) {
		return new TupleQuery<>(this, selections);
	}

	@Override
	public <T> ISingleQuery<F, T> select(final TupleMapper<T> mapper, final ISelection<?>... selections) {
		return new TupleMappedQuery<>(this, mapper, selections);
	}

	@Override
	public <T> ISingleQuery<F, T> select(final MappedSelectionBuilder<?, T> selectionBuilder) {
		return new TupleMappedQuery<>(this, selectionBuilder);
	}

	protected void prepareSubqueries() {
		if (this.subqueries != null) {
			for (final Entry<ISubquery<?, ?>, Subquery<?>> entry : this.subqueries.entrySet()) {
				entry.setValue(entry.getKey().create(this));
			}
		}
	}

	protected abstract void prepareSelect(CriteriaQuery<R> criteriaQuery);

	protected void prepareFrom(final CriteriaQuery<R> criteriaQuery) {
		for (final Entry<IFrom<?, ?>, From<?, ?>> entry : this.froms.entrySet()) {
			entry.setValue(entry.getKey().create(this, criteriaQuery));
		}
	}

	protected void prepareWhere(final CriteriaQuery<R> criteriaQuery) {
		if (this.predicates != null && !this.predicates.isEmpty()) {
			criteriaQuery.where(this.predicates.stream().map(p -> p.resolve(this)).toArray(Predicate[]::new));
		}
	}

	protected void prepareOrder(final CriteriaQuery<R> criteriaQuery) {
		if (this.orders != null && !this.orders.isEmpty()) {
			criteriaQuery.orderBy(this.orders.stream().map(o -> o.resolve(this)).collect(toList()));
		}
	}

	protected void prepareGroup(final CriteriaQuery<R> criteriaQuery) {
		if (this.groups != null && !this.groups.isEmpty()) {
			criteriaQuery.groupBy(this.groups.stream().map(g -> g.resolve(this)).collect(toList()));
		}
	}

	protected void prepareHaving(final CriteriaQuery<R> criteriaQuery) {
		if (this.havingPredicates != null && !this.havingPredicates.isEmpty()) {
			criteriaQuery.having(this.havingPredicates.stream().map(p -> p.resolve(this)).toArray(Predicate[]::new));
		}
	}

	protected abstract Class<R> getResultClass();

	protected TypedQuery<R> prepareQuery() {
		if (this.invalidated) {
			throw new IllegalStateException("Methods select or fetch have been called, this query class instance should not be used");
		}
		final CriteriaQuery<R> criteriaQuery = this.criteriaBuilder.createQuery(this.getResultClass());
		criteriaQuery.distinct(this.distinct);
		try {
			// XXX not thread-safe but needed while resolving expressions
			this.currentQuery = criteriaQuery;
			this.prepareFrom(criteriaQuery);
			// this.prepareSubqueries();
			this.prepareSelect(criteriaQuery);
			this.prepareWhere(criteriaQuery);
			this.prepareOrder(criteriaQuery);
			this.prepareGroup(criteriaQuery);
			this.prepareHaving(criteriaQuery);
		} finally {
			this.currentQuery = null;
		}
		final TypedQuery<R> query = this.entityManager.createQuery(criteriaQuery);
		if (this.lockMode != null) {
			query.setLockMode(this.lockMode);
		}
		if (this.flushMode != null) {
			query.setFlushMode(this.flushMode);
		}
		if (this.limit >= 0) {
			query.setMaxResults(this.limit);
		}
		if (this.offset >= 0) {
			query.setFirstResult(this.offset);
		}
		if (this.hints != null) {
			this.hints.forEach(query::setHint);
		}
		this.invalidateBeforeQuery();
		return query;
	}

	@Override
	public CriteriaBuilder getCriteriaBuilder() {
		return this.criteriaBuilder;
	}

	@Override
	public AbstractQuery<?> getCurrentQuery() {
		return this.currentQuery;
	}

	@Override
	public Q distinct() {
		this.distinct = true;
		return (Q) this;
	}

	protected void fromInternal(final @NonNull IRoot<F> root, final IFrom<?, ?>... froms) {
		this.froms.clear();
		this.mainRoot = requireNonNull(root);
		this.addFrom(root);
		for (final IFrom<?, ?> from : froms) {
			this.addFrom(from);
		}
	}

	@Override
	public <S, T> From<S, T> getFrom(final IFrom<S, T> from) {
		final String identifier = from.getIdentifier();
		if (identifier != null) {
			return (From<S, T>) this.fromsById.get(identifier);
		}
		return (From<S, T>) this.froms.get(from);
	}

	@Override
	public <R> Subquery<R> getSubquery(final ISubquery<?, R> subquery, final Supplier<Subquery<R>> supplier) {
		if (this.subqueries == null) {
			this.subqueries = new IdentityHashMap<>();
		}
		return (Subquery<R>) this.subqueries.computeIfAbsent(subquery, k -> supplier.get());
	}

	@Override
	public <S, T> void registerJoin(final IJoin<S, T> iJoin, final Join<S, T> join) {
		final String identifier = iJoin.getIdentifier();
		if (identifier != null) {
			final From<?, ?> existingJoin = this.fromsById.get(identifier);
			if (existingJoin != null) {
				if (join != existingJoin) {
					throw new IllegalStateException("Duplicate identifier: " + identifier);
				}
				return;
			}
			this.fromsById.put(identifier, join);
		}
		this.froms.put(iJoin, join);
	}

	@Override
	public IRoot<?> getMainRoot() {
		return this.mainRoot;
	}

	@Override
	public Q where(final IPredicate... predicates) {
		Collections.addAll(this.getPredicates(), predicates);
		return (Q) this;
	}

	@Override
	public Q orderBy(final IOrder... orders) {
		Collections.addAll(this.getOrders(), orders);
		return (Q) this;
	}

	@Override
	public Q groupBy(final IExpression<?>... expressions) {
		if (this.groups == null) {
			this.groups = new ArrayList<>();
		}
		Collections.addAll(this.groups, expressions);
		return (Q) this;
	}

	@Override
	public Q having(final IPredicate... predicates) {
		if (this.havingPredicates == null) {
			this.havingPredicates = new ArrayList<>();
		}
		Collections.addAll(this.havingPredicates, predicates);
		return (Q) this;
	}

	@Override
	public Q lockMode(final LockModeType lockMode) {
		this.lockMode = lockMode;
		return (Q) this;
	}

	@Override
	public Q flushMode(final FlushModeType flushMode) {
		this.flushMode = flushMode;
		return (Q) this;
	}

	@Override
	public Q limit(final int limit) {
		this.limit = limit;
		return (Q) this;
	}

	@Override
	public Q offset(final int offset) {
		this.offset = offset;
		return (Q) this;
	}

	@Override
	public Q hint(final String key, final Object value) {
		if (this.hints == null) {
			this.hints = new HashMap<>();
		}
		this.hints.put(key, value);
		return (Q) this;
	}

	@Override
	public <V> Q eq(final IExpression<V> expr, final V value) {
		this.getPredicates().add(Expressions.equalOrNull(expr, value));
		return (Q) this;
	}

	@Override
	public <V> Q in(final IExpression<V> expression, final Collection<? extends V> values) {
		if (values == null) {
			this.getPredicates().add(expression.isNull());
		} else if (values.isEmpty()) {
			this.getPredicates().add(Expressions.disjunction());
		} else {
			this.getPredicates().add(expression.in(values));
		}
		return (Q) this;
	}

	@Override
	public Q order(final boolean desc, final IExpression<?> expression) {
		this.getOrders().add(desc ? expression.desc() : expression.asc());
		return (Q) this;
	}

	private void addFrom(final IFrom<?, ?> from) {
		final String identifier = from.getIdentifier();
		if (identifier != null) {
			if (this.fromsById.containsKey(identifier)) {
				throw new IllegalStateException("Duplicate identifier " + identifier);
			}
		}
		this.froms.put(from, null);
	}

	private List<IPredicate> getPredicates() {
		if (this.predicates == null) {
			this.predicates = new ArrayList<>();
		}
		return this.predicates;
	}

	private List<IOrder> getOrders() {
		if (this.orders == null) {
			this.orders = new ArrayList<>();
		}
		return this.orders;
	}

	private static <T> Selection<T> compoundSelection(final IQueryDataHolder data, final Class<T> resultClass, final ISelection<?>... selections) {
		@SuppressWarnings("rawtypes")
		final Selection[] compoundSelection = new Selection[selections.length];
		for (int i = 0; i < selections.length; i++) {
			compoundSelection[i] = selections[i].resolve(data);
		}
		return data.getCriteriaBuilder().construct(resultClass, compoundSelection);
	}
}
