package dev.tacon.jakartaee.query;

import static java.util.stream.Collectors.toCollection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee.query.ITupleQuery.TupleResolver;
import dev.tacon.jakartaee.query.criteria.IFrom;
import dev.tacon.jakartaee.query.criteria.IRoot;
import dev.tacon.jakartaee.query.criteria.ISelection;
import jakarta.persistence.Tuple;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Selection;

class TupleQuery<F> extends Query<ITupleQuery<F>, F, Tuple> implements ITupleQuery<F>, TupleResolver {

	private final List<ISelection<?>> iSelections;
	private Map<ISelection<?>, Selection<?>> selectionsMap;

	TupleQuery(final Query<?, F, ?> query, final ISelection<?>... selections) {
		super(query);
		this.iSelections = new ArrayList<>(selections.length);
		Collections.addAll(this.iSelections, selections);
	}

	@Override
	protected Class<Tuple> getResultClass() {
		return Tuple.class;
	}

	@Override
	protected void prepareSelect(final CriteriaQuery<Tuple> criteriaQuery) {
		this.selectionsMap = new IdentityHashMap<>();
		final List<Selection<?>> selections = new ArrayList<>(this.iSelections.size());
		for (final ISelection<?> iSelection : this.iSelections) {
			final Selection<?> selection = iSelection.resolve(this);
			this.selectionsMap.put(iSelection, selection);
			selections.add(selection);
		}
		criteriaQuery.multiselect(selections);
	}

	@Override
	protected void invalidateBeforeQuery() {
		// invalidation will be done manually
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> ITupleQuery<T> from(final IRoot<T> root, final IFrom<?, ?>... froms) {
		final TupleQuery<T> query = (TupleQuery<T>) this;
		query.fromInternal(root, froms);
		return query;
	}

	@Override
	public <R> List<R> fetch(final TupleMapper<R> mapper) {
		return this.prepareQuery().getResultStream()
				.onClose(this::invalidate)
				.map(tuple -> mapper.map(tuple, this))
				.collect(toCollection(ArrayList::new));
	}

	@Override
	public void fetchAndThen(final TupleConsumer consumer) {
		this.prepareQuery().getResultStream().forEach(t -> consumer.accept(t, this));
		this.invalidate();
	}

	@Override
	public <R> R fetchAndThenMap(final BiFunction<Stream<Tuple>, TupleResolver, R> mapper) {
		return mapper.apply(this.prepareQuery().getResultStream().onClose(this::invalidate), this);
	}

	@Override
	public <R> Optional<R> fetchFirst(final TupleMapper<R> mapper) {
		final TypedQuery<Tuple> query = this.prepareQuery();
		query.setMaxResults(1);
		final Optional<R> optional = query.getResultStream().findFirst().map(tuple -> mapper.map(tuple, this));
		this.invalidate();
		return optional;
	}

	@Override
	public <R> Stream<R> stream(final TupleMapper<R> mapper) {
		return this.prepareQuery().getResultStream()
				.map(tuple -> mapper.map(tuple, this))
				.onClose(this::invalidate);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T get(final @NonNull Tuple tuple, final @NonNull ISelection<T> selection) {
		final Selection<T> sel = (Selection<T>) this.selectionsMap.get(selection);
		return tuple.get(sel != null ? sel : selection.resolve(this));
	}
}
