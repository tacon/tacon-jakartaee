package dev.tacon.jakartaee.query;

import java.util.Collection;

import dev.tacon.jakartaee.query.ITupleQuery.TupleMapper;
import dev.tacon.jakartaee.query.criteria.IExpression;
import dev.tacon.jakartaee.query.criteria.IOrder;
import dev.tacon.jakartaee.query.criteria.IPredicate;
import dev.tacon.jakartaee.query.criteria.ISelection;
import jakarta.persistence.FlushModeType;
import jakarta.persistence.LockModeType;
import jakarta.persistence.metamodel.SingularAttribute;

public interface IQuery<Q, F> {

	record Tuple2<T1, T2>(T1 field1, T2 field2) {}

	record Tuple3<T1, T2, T3>(T1 field1, T2 field2, T3 field3) {}

	record Tuple4<T1, T2, T3, T4>(T1 field1, T2 field2, T3 field, T4 field4) {}

	record Tuple5<T1, T2, T3, T4, T5>(T1 field1, T2 field2, T3 field3, T4 field4, T5 field5) {}

	record Tuple6<T1, T2, T3, T4, T5, T6>(T1 field1, T2 field2, T3 field3, T4 field4, T5 field5, T6 field6) {}

	record Tuple7<T1, T2, T3, T4, T5, T6, T7>(T1 field1, T2 field2, T3 field3, T4 field4, T5 field5, T6 field6, T7 field7) {}

	record Tuple8<T1, T2, T3, T4, T5, T6, T7, T8>(T1 field1, T2 field2, T3 field3, T4 field4, T5 field5, T6 field6, T7 field7, T8 field8) {}

	record Tuple9<T1, T2, T3, T4, T5, T6, T7, T8, T9>(T1 field1, T2 field2, T3 field3, T4 field4, T5 field5, T6 field6, T7 field7, T8 field8, T9 field9) {}

	record Tuple10<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(T1 field1, T2 field2, T3 field3, T4 field4, T5 field5, T6 field6, T7 field7, T8 field8, T9 field9, T10 field10) {}

	/**
	 * Specify that duplicate query results will be eliminated.
	 *
	 * @return the modified query.
	 */
	Q distinct();

	/**
	 * Specify the selection item that is to be returned in the
	 * query result.
	 * Replaces the previously specified selection(s), if any.
	 *
	 * @param resultClass type of the final result
	 * @param selection selection item corresponding to the
	 *            result to be returned by the query
	 *
	 * @return the modified query
	 */
	<T> ISingleQuery<F, T> select(Class<T> resultClass, ISelection<T> selection);

	/**
	 * Specify the compound selection that is to be returned in the
	 * query result.
	 * Replaces the previously specified selection(s), if any.
	 *
	 * @param resultClass type of the final result
	 * @param selection selection items corresponding to the
	 *            result to be returned by the query
	 *
	 * @return the modified query
	 */
	<T> ISingleQuery<F, T> select(Class<T> resultClass, ISelection<?>... selections);

	/**
	 * Specify the selection item that is to be returned in the
	 * query result.
	 * Replaces the previously specified selection(s), if any.
	 *
	 * @param attribute selection item corresponding to the
	 *            result to be returned by the query,
	 *            resolved with {@code Expressions.path(attribute)}
	 *
	 * @return the modified query
	 */
	default <T> ISingleQuery<F, T> select(final SingularAttribute<? super F, T> attribute) {
		return this.select(attribute.getJavaType(), Expressions.path(attribute));
	}

	/**
	 * Specify the selection items that are to be returned in the
	 * query result.
	 * Replaces the previously specified selection(s), if any.
	 *
	 * @param selections selection items corresponding to the
	 *            results to be returned by the query
	 *
	 * @return the modified query to work with tuples
	 */
	ITupleQuery<F> select(ISelection<?>... selections);

	/**
	 * Specify the selection items that are to be returned in the
	 * query result.
	 * Replaces the previously specified selection(s), if any.
	 * <p>
	 * A mapper must also be specified to tell how
	 * the selection has to be mapped to final result elements.
	 *
	 * <pre>
	 * IPath&lt;Integer&gt; _id = ... ;
	 * IPath&lt;String&gt; _surname = ... ;
	 * IPath&lt;String&gt; _name = ... ;
	 * ...
	 * query.select((tuple, resolver) -> new PersonDTO(
	 * 	resolver.get(tuple, _id),
	 * 	resolver.get(tuple, _surname),
	 * 	resolver.get(tuple, _name)),
	 * 	_id, _surname, _name);
	 * </pre>
	 *
	 * @param mapper the function which maps a tuple
	 *            to the final result element.
	 * @param selections selection items corresponding to the
	 *            results to be returned by the query
	 *
	 * @return the modified query to work with tuples
	 */
	<T> ISingleQuery<F, T> select(TupleMapper<T> mapper, ISelection<?>... selections);

	/**
	 * Specify the selection items that are to be returned in the
	 * query result through a {@linkplain MappedSelectionBuilder}.
	 * Replaces the previously specified selection(s), if any.
	 *
	 * <pre>
	 * IPath&lt;Integer&gt; _id = ... ;
	 * IPath&lt;String&gt; _surname = ... ;
	 * IPath&lt;String&gt; _name = ... ;
	 * ...
	 *
	 * // constructor with no arguments
	 * query.select(
	 * 	MappedSelectionBuilder.newBuilder(PersonDTO::new)
	 * 		.add(_id, PersonDTO::setId)
	 * 		.add(_surname, PersonDTO::setSurname)
	 * 		.add(_name, PersonDTO::setName));
	 *
	 * // constructor with one argument
	 * query.select(
	 * 	MappedSelectionBuilder.newBuilder(
	 * 		_id, id -> new PersonDTO(id))
	 * 		.add(_surname, PersonDTO::setSurname)
	 * 		.add(_name, PersonDTO::setName));
	 * </pre>
	 *
	 * @param mapper the function which maps a tuple
	 *            to the final result element.
	 * @param selections selection items corresponding to the
	 *            results to be returned by the query
	 *
	 * @return the modified query
	 */
	<T> ISingleQuery<F, T> select(MappedSelectionBuilder<?, T> selectionBuilder);

	/**
	 * Specify the 2 selection items that are to be returned in the
	 * query result.
	 * Replaces the previously specified selection(s), if any.
	 * <p>
	 * The final result will be provided through a type-safe
	 * tuple of 2 values.
	 *
	 * <pre>
	 * IPath&lt;String&gt; _code = ... ;
	 * IPath&lt;String&gt; _description = ... ;
	 * ...
	 * query.selectTuple(_code, _description)
	 * ...
	 * .stream().map(t -> new CodeDescription(t.field1(), t.field2()));
	 * </pre>
	 *
	 * @return the modified query
	 */
	default <T1, T2> ISingleQuery<F, Tuple2<T1, T2>> selectTuple(final ISelection<T1> sel1, final ISelection<T2> sel2) {
		final Class<Tuple2<T1, T2>> resultClass = uncheckedCast(Tuple2.class);
		return this.select(resultClass, sel1, sel2);
	}

	/**
	 * Specify the 3 selection items that are to be returned in the
	 * query result.
	 * Replaces the previously specified selection(s), if any.
	 * <p>
	 * The final result will be provided through a type-safe
	 * tuple of 3 values.
	 *
	 * @return the modified query
	 */
	default <T1, T2, T3> ISingleQuery<F, Tuple3<T1, T2, T3>> selectTuple(final ISelection<T1> sel1, final ISelection<T2> sel2, final ISelection<T3> sel3) {
		final Class<Tuple3<T1, T2, T3>> resultClass = uncheckedCast(Tuple3.class);
		return this.select(resultClass, sel1, sel2, sel3);
	}

	/**
	 * Specify the 4 selection items that are to be returned in the
	 * query result.
	 * Replaces the previously specified selection(s), if any.
	 * <p>
	 * The final result will be provided through a type-safe
	 * tuple of 4 values.
	 *
	 * @return the modified query
	 */
	default <T1, T2, T3, T4> ISingleQuery<F, Tuple4<T1, T2, T3, T4>> selectTuple(final ISelection<T1> sel1, final ISelection<T2> sel2, final ISelection<T3> sel3, final ISelection<T4> sel4) {
		final Class<Tuple4<T1, T2, T3, T4>> resultClass = uncheckedCast(Tuple4.class);
		return this.select(resultClass, sel1, sel2, sel3, sel4);
	}

	/**
	 * Specify the 5 selection items that are to be returned in the
	 * query result.
	 * Replaces the previously specified selection(s), if any.
	 * <p>
	 * The final result will be provided through a type-safe
	 * tuple of 5 values.
	 *
	 * @return the modified query
	 */
	default <T1, T2, T3, T4, T5> ISingleQuery<F, Tuple5<T1, T2, T3, T4, T5>> selectTuple(final ISelection<T1> sel1, final ISelection<T2> sel2, final ISelection<T3> sel3, final ISelection<T4> sel4, final ISelection<T5> sel5) {
		final Class<Tuple5<T1, T2, T3, T4, T5>> resultClass = uncheckedCast(Tuple5.class);
		return this.select(resultClass, sel1, sel2, sel3, sel4, sel5);
	}

	/**
	 * Specify the 6 selection items that are to be returned in the
	 * query result.
	 * Replaces the previously specified selection(s), if any.
	 * <p>
	 * The final result will be provided through a type-safe
	 * tuple of 6 values.
	 *
	 * @return the modified query
	 */
	default <T1, T2, T3, T4, T5, T6> ISingleQuery<F, Tuple6<T1, T2, T3, T4, T5, T6>> selectTuple(final ISelection<T1> sel1, final ISelection<T2> sel2, final ISelection<T3> sel3, final ISelection<T4> sel4, final ISelection<T5> sel5, final ISelection<T6> sel6) {
		final Class<Tuple6<T1, T2, T3, T4, T5, T6>> resultClass = uncheckedCast(Tuple6.class);
		return this.select(resultClass, sel1, sel2, sel3, sel4, sel5, sel6);
	}

	/**
	 * Specify the 7 selection items that are to be returned in the
	 * query result.
	 * Replaces the previously specified selection(s), if any.
	 * <p>
	 * The final result will be provided through a type-safe
	 * tuple of 7 values.
	 *
	 * @return the modified query
	 */
	default <T1, T2, T3, T4, T5, T6, T7> ISingleQuery<F, Tuple7<T1, T2, T3, T4, T5, T6, T7>> selectTuple(final ISelection<T1> sel1, final ISelection<T2> sel2, final ISelection<T3> sel3, final ISelection<T4> sel4, final ISelection<T5> sel5, final ISelection<T6> sel6, final ISelection<T7> sel7) {
		final Class<Tuple7<T1, T2, T3, T4, T5, T6, T7>> resultClass = uncheckedCast(Tuple7.class);
		return this.select(resultClass, sel1, sel2, sel3, sel4, sel5, sel6, sel7);
	}

	/**
	 * Specify the 8 selection items that are to be returned in the
	 * query result.
	 * Replaces the previously specified selection(s), if any.
	 * <p>
	 * The final result will be provided through a type-safe
	 * tuple of 8 values.
	 *
	 * @return the modified query
	 */
	default <T1, T2, T3, T4, T5, T6, T7, T8> ISingleQuery<F, Tuple8<T1, T2, T3, T4, T5, T6, T7, T8>> selectTuple(final ISelection<T1> sel1, final ISelection<T2> sel2, final ISelection<T3> sel3, final ISelection<T4> sel4, final ISelection<T5> sel5, final ISelection<T6> sel6, final ISelection<T7> sel7, final ISelection<T8> sel8) {
		final Class<Tuple8<T1, T2, T3, T4, T5, T6, T7, T8>> resultClass = uncheckedCast(Tuple8.class);
		return this.select(resultClass, sel1, sel2, sel3, sel4, sel5, sel6, sel7, sel8);
	}

	/**
	 * Specify the 9 selection items that are to be returned in the
	 * query result.
	 * Replaces the previously specified selection(s), if any.
	 * <p>
	 * The final result will be provided through a type-safe
	 * tuple of 9 values.
	 *
	 * @return the modified query
	 */
	default <T1, T2, T3, T4, T5, T6, T7, T8, T9> ISingleQuery<F, Tuple9<T1, T2, T3, T4, T5, T6, T7, T8, T9>> selectTuple(final ISelection<T1> sel1, final ISelection<T2> sel2, final ISelection<T3> sel3, final ISelection<T4> sel4, final ISelection<T5> sel5, final ISelection<T6> sel6, final ISelection<T7> sel7, final ISelection<T8> sel8, final ISelection<T9> sel9) {
		final Class<Tuple9<T1, T2, T3, T4, T5, T6, T7, T8, T9>> resultClass = uncheckedCast(Tuple9.class);
		return this.select(resultClass, sel1, sel2, sel3, sel4, sel5, sel6, sel7, sel8, sel9);
	}

	/**
	 * Specify the 10 selection items that are to be returned in the
	 * query result.
	 * Replaces the previously specified selection(s), if any.
	 * <p>
	 * The final result will be provided through a type-safe
	 * tuple of 10 values.
	 *
	 * @return the modified query
	 */
	default <T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> ISingleQuery<F, Tuple10<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>> selectTuple(final ISelection<T1> sel1, final ISelection<T2> sel2, final ISelection<T3> sel3, final ISelection<T4> sel4, final ISelection<T5> sel5, final ISelection<T6> sel6, final ISelection<T7> sel7, final ISelection<T8> sel8, final ISelection<T9> sel9, final ISelection<T10> sel10) {
		final Class<Tuple10<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>> resultClass = uncheckedCast(Tuple10.class);
		return this.select(resultClass, sel1, sel2, sel3, sel4, sel5, sel6, sel7, sel8, sel9, sel10);
	}

	/**
	 * Modify the query to restrict the query result according
	 * to the conjunction of the specified restriction predicates
	 * and the previously added restriction(s), if any.
	 *
	 * @param predicates zero or more restriction predicates
	 * @return the modified query
	 */
	Q where(IPredicate... predicates);

	/**
	 * Specify the ordering expressions that are used to
	 * order the query results.
	 * These expressions will be added to the previous
	 * ones, if any.
	 * The left-to-right sequence of the ordering expressions
	 * determines the precedence, whereby the leftmost has highest
	 * precedence.
	 *
	 * @param orders zero or more ordering expressions
	 * @return the modified query
	 */
	Q orderBy(IOrder... orders);

	/**
	 * Specify the expressions that are used to form groups over
	 * the query results.
	 * These expressions will be added to the previous
	 * ones, if any.
	 *
	 * @param expressions zero or more grouping expressions
	 * @return the modified query
	 */
	Q groupBy(IExpression<?>... expressions);

	/**
	 * Specify restrictions over the groups of the query
	 * to the conjunction of the specified restriction predicates
	 * and the previously added restriction(s), if any.
	 *
	 * @param predicates zero or more restriction predicates
	 * @return the modified query
	 */
	Q having(IPredicate... predicates);

	/**
	 * Set the lock mode type to be used for the query execution.
	 *
	 * @param lockMode lock mode
	 * @return the same query instance
	 */
	Q lockMode(LockModeType lockMode);

	/**
	 * Set the flush mode type to be used for the query execution.
	 * The flush mode type applies to the query regardless of the
	 * flush mode type in use for the entity manager.
	 *
	 * @param flushMode flush mode
	 * @return the same query instance
	 */
	Q flushMode(FlushModeType flushMode);

	/**
	 * Set the maximum number of results to retrieve.
	 * If the number is negative this limit will not
	 * be applied.
	 *
	 * @param limit maximum number of results to retrieve
	 * @return the same query instance
	 */
	Q limit(int limit);

	/**
	 * Set the position of the first result to retrieve.
	 * If the number is negative this offset will not
	 * be applied.
	 *
	 * @param startPosition position of the first result
	 * @return the same query instance
	 */
	Q offset(int offset);

	/**
	 * Set a query property or hint. The hints elements may be used
	 * to specify query properties and hints.
	 *
	 * @param hintName name of property or hint
	 * @param value value for the property or hint
	 * @return the same query instance
	 * @throws IllegalArgumentException if the second argument is not
	 *             valid for the implementation
	 */
	Q hint(String key, Object value);

	/**
	 * Restricts the query results applying a
	 * predicate for testing the specified expression
	 * against specified value, if the value is not {@code null}.
	 * <p>If the value is {@code null} the predicate will
	 * test whether the expression is {@code null}.</p>
	 *
	 * @param <V> the type of the expression
	 * @param expression expression to be tested
	 */
	<V> Q eq(IExpression<V> expression, V value);

	/**
	 * Restricts the query results applying a
	 * predicate for testing if the specified expression
	 * is contained the specified collection of values,
	 * if the collection is not {@code null} nor empty.
	 * <p>If the collection is {@code null} the predicate will
	 * test whether the expression is {@code null}.</p>
	 * <p>If the collection is empty the predicate
	 * will be a disjunction.</p>
	 *
	 * @param <V> the type of the expression
	 * @param values collection of values to be tested against
	 */
	<V> Q in(IExpression<V> expression, Collection<? extends V> values);

	default <V> Q eq(final SingularAttribute<? super F, V> attribute, final V value) {
		return this.eq(Expressions.path(attribute), value);
	}

	default <V> Q in(final SingularAttribute<? super F, V> attribute, final Collection<? extends V> values) {
		return this.in(Expressions.path(attribute), values);
	}

	default Q eq(final SingularAttribute<? super F, Boolean> attribute, final boolean value) {
		return this.eq(attribute, Boolean.valueOf(value));
	}

	default Q eq(final SingularAttribute<? super F, Integer> attribute, final int value) {
		return this.eq(attribute, Integer.valueOf(value));
	}

	default Q eq(final SingularAttribute<? super F, Long> attribute, final long value) {
		return this.eq(attribute, Long.valueOf(value));
	}

	default <X, V> Q eq(final SingularAttribute<? super F, X> attribute1, final SingularAttribute<? super X, V> attribute2, final V value) {
		return this.eq(Expressions.path(attribute1, attribute2), value);
	}

	default <X, Y, V> Q eq(final SingularAttribute<? super F, X> attribute1, final SingularAttribute<? super X, Y> attribute2, final SingularAttribute<? super Y, V> attribute3, final V value) {
		return this.eq(Expressions.path(attribute1, attribute2, attribute3), value);
	}

	default Q order(final IExpression<?> expression) {
		return this.order(false, expression);
	}

	Q order(boolean desc, IExpression<?> expression);

	default Q order(final SingularAttribute<? super F, ?> attribute) {
		return this.order(false, attribute);
	}

	default Q order(final boolean desc, final SingularAttribute<? super F, ?> attribute) {
		return this.order(desc, Expressions.path(attribute));
	}

	default <X, V> Q order(final boolean desc, final SingularAttribute<? super F, X> attribute1, final SingularAttribute<? super X, V> attribute2) {
		return this.order(desc, Expressions.path(attribute1, attribute2));
	}

	default <X, Y, V> Q order(final boolean desc, final SingularAttribute<? super F, X> attribute1, final SingularAttribute<? super X, Y> attribute2, final SingularAttribute<? super Y, V> attribute3) {
		return this.order(desc, Expressions.path(attribute1, attribute2, attribute3));
	}

	@SuppressWarnings("unchecked")
	private static <T> Class<T> uncheckedCast(final Class<?> c) {
		return (Class<T>) c;
	}
}
