package dev.tacon.jakartaee.query;

import static java.util.Objects.requireNonNull;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee.query.ITupleQuery.TupleMapper;
import dev.tacon.jakartaee.query.criteria.ISelection;

public class MappedSelectionBuilder<X, R> {

	public static <X, R> MappedSelectionBuilder<X, R> newBuilder(
			final @NonNull ISelection<? extends X> selection,
			final @NonNull Function<X, R> instanceProvider) {
		return new MappedSelectionBuilder<>(requireNonNull(selection), requireNonNull(instanceProvider));
	}

	public static <R> MappedSelectionBuilder<Void, R> newBuilder(final @NonNull Supplier<R> instanceProvider) {
		requireNonNull(instanceProvider);
		return new MappedSelectionBuilder<>(null, __ -> instanceProvider.get());
	}

	private final Function<X, R> instanceProvider;
	private final ISelection<? extends X> firstSelection;
	private final List<ISelection<?>> selections = new LinkedList<>();
	private final List<BiConsumer<R, ?>> setters = new LinkedList<>();

	private MappedSelectionBuilder(final ISelection<? extends X> selection, final Function<X, R> instanceProvider) {
		this.instanceProvider = instanceProvider;
		this.firstSelection = selection;
		if (selection != null) {
			this.selections.add(selection);
		}
	}

	public <T> MappedSelectionBuilder<X, R> add(final ISelection<? extends T> selection, final BiConsumer<R, T> setter) {
		this.selections.add(selection);
		this.setters.add(setter);
		return this;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	Map.Entry<TupleMapper<R>, List<ISelection<?>>> build() {
		final TupleMapper<R> mapper = (tuple, resolver) -> {
			final Iterator<ISelection<?>> itSelection = this.selections.iterator();
			final Iterator<BiConsumer<R, ?>> itSetter = this.setters.iterator();
			final R result = this.instanceProvider.apply(resolver.get(tuple, this.firstSelection));
			if (this.firstSelection != null) {
				itSelection.next();
			}
			while (itSelection.hasNext()) {
				final ISelection selection = itSelection.next();
				final BiConsumer setter = itSetter.next();
				setter.accept(result, resolver.get(tuple, selection));
			}
			return result;
		};
		return new SimpleImmutableEntry<>(mapper, new ArrayList<>(this.selections));
	}
}
