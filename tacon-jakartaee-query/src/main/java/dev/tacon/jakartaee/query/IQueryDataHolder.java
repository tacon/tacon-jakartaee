package dev.tacon.jakartaee.query;

import java.util.function.Supplier;

import dev.tacon.jakartaee.query.criteria.IFrom;
import dev.tacon.jakartaee.query.criteria.IJoin;
import dev.tacon.jakartaee.query.criteria.IRoot;
import jakarta.persistence.criteria.AbstractQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.From;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Subquery;

public interface IQueryDataHolder {

	CriteriaBuilder getCriteriaBuilder();

	AbstractQuery<?> getCurrentQuery();

	<S, T> void registerJoin(IJoin<S, T> iJoin, Join<S, T> join);

	<S, T> From<S, T> getFrom(IFrom<S, T> from);

	<R> Subquery<R> getSubquery(ISubquery<?, R> subquery, Supplier<Subquery<R>> supplier);

	IRoot<?> getMainRoot();
}
