package dev.tacon.jakartaee.query.criteria;

import dev.tacon.jakartaee.query.IQueryDataHolder;
import jakarta.persistence.criteria.Selection;

@FunctionalInterface
public interface ISelection<T> {

	Selection<T> resolve(IQueryDataHolder data);
}
