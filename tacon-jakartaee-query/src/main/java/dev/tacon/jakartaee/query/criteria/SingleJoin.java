package dev.tacon.jakartaee.query.criteria;

import dev.tacon.annotations.Nullable;
import jakarta.persistence.criteria.From;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.metamodel.SingularAttribute;

class SingleJoin<S, T> extends BaseJoin<S, T, SingularAttribute<? super S, T>, Join<S, T>> {

	SingleJoin(final IFrom<?, S> parent, final SingularAttribute<? super S, T> attribute, final JoinType joinType) {
		super(parent, attribute, joinType);
	}

	SingleJoin(final IFrom<?, S> parent, final SingularAttribute<? super S, T> attribute, final JoinType joinType, final @Nullable String identifier) {
		super(parent, attribute, joinType, identifier);
	}

	@Override
	protected Join<S, T> create(final From<?, S> from) {
		return from.join(this.attribute, this.joinType);
	}
}
