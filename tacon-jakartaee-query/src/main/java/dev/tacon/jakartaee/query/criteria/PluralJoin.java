package dev.tacon.jakartaee.query.criteria;

import dev.tacon.annotations.Nullable;
import jakarta.persistence.criteria.From;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.metamodel.CollectionAttribute;
import jakarta.persistence.metamodel.ListAttribute;
import jakarta.persistence.metamodel.MapAttribute;
import jakarta.persistence.metamodel.PluralAttribute;
import jakarta.persistence.metamodel.SetAttribute;

class PluralJoin<A extends PluralAttribute<? super X, C, E>, X, C, E> extends BaseJoin<X, E, A, Join<X, E>> {

	PluralJoin(final IFrom<?, X> parent, final A attribute, final JoinType joinType) {
		super(parent, attribute, joinType);
	}

	PluralJoin(final IFrom<?, X> parent, final A attribute, final JoinType joinType, final @Nullable String identifier) {
		super(parent, attribute, joinType, identifier);
	}

	@Override
	public @Nullable String getIdentifier() {
		return this.identifier;
	}

	@Override
	@SuppressWarnings("unchecked")
	protected Join<X, E> create(final From<?, X> from) {
		final Join<X, E> join = switch (this.attribute.getCollectionType()) {
			case COLLECTION -> from.join((CollectionAttribute<? super X, E>) this.attribute, this.joinType);
			case LIST -> from.join((ListAttribute<? super X, E>) this.attribute, this.joinType);
			case MAP -> from.join((MapAttribute<? super X, ?, E>) this.attribute, this.joinType);
			case SET -> from.join((SetAttribute<? super X, E>) this.attribute, this.joinType);
			default -> throw new IllegalArgumentException("Join has invalid collection type");
		};
		return join;
	}
}
