package dev.tacon.jakartaee.query.criteria;

import java.util.function.Function;

import dev.tacon.jakartaee.query.IQueryDataHolder;
import jakarta.persistence.criteria.AbstractQuery;
import jakarta.persistence.criteria.From;
import jakarta.persistence.criteria.Join;

@FunctionalInterface
public interface IJoin<Z, X> extends IFrom<Z, X> {

	@Override
	default From<Z, X> create(final IQueryDataHolder data, final AbstractQuery<?> criteriaQuery) {
		throw new UnsupportedOperationException("Invalid join implementation");
	}

	@Override
	Join<Z, X> resolve(final IQueryDataHolder data);

	/**
	 * <p>ATTENTION</p>
	 * The joins order depends on selected fields.
	 * To force the order add the {@code IJoin} instance
	 * to the from clause.
	 *
	 *
	 * @param condition
	 * @return
	 */
	default IJoin<Z, X> on(final Function<IJoin<Z, X>, IExpression<Boolean>> condition) {
		return data -> this.resolve(data).on(condition.apply(this).resolve(data));
	}
}
