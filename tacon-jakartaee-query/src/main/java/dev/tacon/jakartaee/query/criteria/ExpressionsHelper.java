package dev.tacon.jakartaee.query.criteria;

import java.math.BigDecimal;
import java.util.Map;
import java.util.function.BiFunction;

import dev.tacon.annotations.Nullable;
import dev.tacon.jakartaee.query.IQueryDataHolder;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Predicate;

final class ExpressionsHelper {

	private static final Map<Class<?>, Class<?>> WRAPPERS = Map.of(
			Boolean.TYPE, Boolean.class,
			Byte.TYPE, Byte.class,
			Character.TYPE, Character.class,
			Double.TYPE, Double.class,
			Float.TYPE, Float.class,
			Integer.TYPE, Integer.class,
			Long.TYPE, Long.class,
			Short.TYPE, Short.class,
			Void.TYPE, Void.class);

	@SuppressWarnings("unchecked")
	static Predicate isTrueUnchecked(final IQueryDataHolder data, final IExpression<?> iExpr) {
		final Expression<?> expr = iExpr.resolve(data);
		Class<?> type = expr.getJavaType();
		if (type.isPrimitive()) {
			type = WRAPPERS.get(type);
		}
		if (Boolean.class == type) {
			return data.getCriteriaBuilder().isTrue((Expression<Boolean>) expr);
		}
		if (Number.class.isAssignableFrom(type)) {
			if (Long.class == type) {
				return data.getCriteriaBuilder().equal(expr, Long.valueOf(1));
			}
			if (Integer.class == type) {
				return data.getCriteriaBuilder().equal(expr, Integer.valueOf(1));
			}
			if (Short.class == type) {
				return data.getCriteriaBuilder().equal(expr, Short.valueOf((short) 1));
			}
			if (Byte.class == type) {
				return data.getCriteriaBuilder().equal(expr, Byte.valueOf((byte) 1));
			}
			if (Double.class == type) {
				return data.getCriteriaBuilder().equal(expr, Double.valueOf(1));
			}
			if (Float.class == type) {
				return data.getCriteriaBuilder().equal(expr, Float.valueOf(1F));
			}
			if (BigDecimal.class == type) {
				return data.getCriteriaBuilder().equal(expr, BigDecimal.ONE);
			}
		} else if (String.class == type) {
			return data.getCriteriaBuilder().lower((Expression<String>) expr).in("true", "1");
		}
		if (Character.class == type) {
			return data.getCriteriaBuilder().equal(expr, Character.valueOf('1'));
		}
		throw new IllegalArgumentException("Invalid expression type " + type + " for \"isTrue\" predicate");
	}

	@SuppressWarnings("unchecked")
	static Predicate isFalseUnchecked(final IQueryDataHolder data, final IExpression<?> iExpr) {
		final Expression<?> expr = iExpr.resolve(data);
		Class<?> type = expr.getJavaType();
		if (type.isPrimitive()) {
			type = WRAPPERS.get(type);
		}
		if (Boolean.class == type) {
			return data.getCriteriaBuilder().isFalse((Expression<Boolean>) expr);
		}
		if (Number.class.isAssignableFrom(type)) {
			if (Long.class == type) {
				return data.getCriteriaBuilder().equal(expr, Long.valueOf(0));
			}
			if (Integer.class == type) {
				return data.getCriteriaBuilder().equal(expr, Integer.valueOf(0));
			}
			if (Short.class == type) {
				return data.getCriteriaBuilder().equal(expr, Short.valueOf((short) 0));
			}
			if (Byte.class == type) {
				return data.getCriteriaBuilder().equal(expr, Byte.valueOf((byte) 0));
			}
			if (Double.class == type) {
				return data.getCriteriaBuilder().equal(expr, Double.valueOf(0));
			}
			if (Float.class == type) {
				return data.getCriteriaBuilder().equal(expr, Float.valueOf(0F));
			}
			if (BigDecimal.class == type) {
				return data.getCriteriaBuilder().equal(expr, BigDecimal.ZERO);
			}
		} else if (String.class.isAssignableFrom(type)) {
			return data.getCriteriaBuilder().lower((Expression<String>) expr).in("false", "0");
		}
		if (Character.class == type) {
			return data.getCriteriaBuilder().equal(expr, Character.valueOf('0'));
		}
		throw new IllegalArgumentException("Invalid expression type " + type + " for \"isFalse\" predicate");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	static Predicate lessThanUnchecked(final IQueryDataHolder data, final IExpression<?> iExpr, final @Nullable Comparable value) {
		return compareUnchecked(data, data.getCriteriaBuilder()::lessThan, iExpr, value);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	static Predicate lessThanOrEqualToUnchecked(final IQueryDataHolder data, final IExpression<?> iExpr, final @Nullable Comparable value) {
		return compareUnchecked(data, data.getCriteriaBuilder()::lessThanOrEqualTo, iExpr, value);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	static Predicate greaterThanUnchecked(final IQueryDataHolder data, final IExpression<?> iExpr, final @Nullable Comparable value) {
		return compareUnchecked(data, data.getCriteriaBuilder()::greaterThan, iExpr, value);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	static Predicate greaterThanOrEqualToUnchecked(final IQueryDataHolder data, final IExpression<?> iExpr, final @Nullable Comparable value) {
		return compareUnchecked(data, data.getCriteriaBuilder()::greaterThanOrEqualTo, iExpr, value);
	}

	@SuppressWarnings("rawtypes")
	private static Predicate compareUnchecked(final IQueryDataHolder data, final BiFunction<Expression, Comparable, Predicate> predicateProvider, final IExpression<?> iExpr, final @Nullable Comparable value) {
		final Expression<?> expr = iExpr.resolve(data);
		Class<?> type = expr.getJavaType();
		if (type.isPrimitive()) {
			type = WRAPPERS.get(type);
		}
		if (Comparable.class.isAssignableFrom(type)) {
			return predicateProvider.apply(expr, value);
		}
		throw new IllegalArgumentException("Invalid expression type " + type + " for \"lessThan\" predicate, must be comparable");
	}

	private ExpressionsHelper() {
		throw new AssertionError();
	}
}
