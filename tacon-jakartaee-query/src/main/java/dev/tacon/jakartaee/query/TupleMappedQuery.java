package dev.tacon.jakartaee.query;

import static java.util.stream.Collectors.toCollection;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Stream;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee.query.ITupleQuery.TupleMapper;
import dev.tacon.jakartaee.query.ITupleQuery.TupleResolver;
import dev.tacon.jakartaee.query.criteria.IFrom;
import dev.tacon.jakartaee.query.criteria.IRoot;
import dev.tacon.jakartaee.query.criteria.ISelection;
import jakarta.persistence.Tuple;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Selection;

class TupleMappedQuery<F, R> extends Query<ISingleQuery<F, R>, F, Tuple> implements ISingleQuery<F, R>, TupleResolver {

	private final IdentityHashMap<ISelection<?>, Selection<?>> selections;
	private final TupleMapper<R> mapper;

	TupleMappedQuery(final Query<?, F, ?> query, final MappedSelectionBuilder<?, R> selectionBuilder) {
		super(query);
		final Map.Entry<TupleMapper<R>, List<ISelection<?>>> mapperAndSelections = selectionBuilder.build();
		this.mapper = mapperAndSelections.getKey();
		this.selections = new IdentityHashMap<>();
		for (final ISelection<?> selection : mapperAndSelections.getValue()) {
			this.selections.put(selection, null);
		}
	}

	TupleMappedQuery(final Query<?, F, ?> query, final TupleMapper<R> mapper, final ISelection<?>... selections) {
		super(query);
		this.mapper = mapper;
		this.selections = new IdentityHashMap<>();
		for (final ISelection<?> selection : selections) {
			this.selections.put(selection, null);
		}
	}

	@Override
	protected Class<Tuple> getResultClass() {
		return Tuple.class;
	}

	@Override
	protected void prepareSelect(final CriteriaQuery<Tuple> criteriaQuery) {
		final List<Selection<?>> l = new ArrayList<>(this.selections.size());
		for (final Entry<ISelection<?>, Selection<?>> entry : this.selections.entrySet()) {
			final Selection<?> selection = entry.getKey().resolve(this);
			entry.setValue(selection);
			l.add(selection);
		}
		criteriaQuery.multiselect(l);
	}

	@Override
	protected void invalidateBeforeQuery() {
		// invalidation will be done manually
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> ISingleQuery<T, R> from(final IRoot<T> root, final IFrom<?, ?>... froms) {
		final TupleMappedQuery<T, R> query = (TupleMappedQuery<T, R>) this;
		query.fromInternal(root, froms);
		return query;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T get(final @NonNull Tuple tuple, final @NonNull ISelection<T> selection) {
		final Selection<T> sel = (Selection<T>) this.selections.get(selection);
		return tuple.get(sel != null ? sel : selection.resolve(this));
	}

	@Override
	public List<R> fetch() {
		return this.prepareQuery().getResultStream()
				.onClose(this::invalidate)
				.map(tuple -> this.mapper.map(tuple, this))
				.collect(toCollection(ArrayList::new));
	}

	@Override
	public Optional<R> fetchFirst() {
		final TypedQuery<Tuple> query = this.prepareQuery();
		query.setMaxResults(1);
		final Optional<R> optional = query.getResultStream().onClose(this::invalidate).findFirst().map(tuple -> this.mapper.map(tuple, this));
		return optional;
	}

	@Override
	public R fetchSingleResult() {
		final TypedQuery<Tuple> query = this.prepareQuery();
		final Tuple singleResult = query.getSingleResult();
		this.invalidate();
		return singleResult != null ? this.mapper.map(singleResult, this) : null;
	}

	@Override
	public Stream<R> stream() {
		return this.prepareQuery().getResultStream().onClose(this::invalidate).map(tuple -> this.mapper.map(tuple, this));
	}
}
