package dev.tacon.jakartaee.persistence.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * An abstract base class for JPA entities, providing basic implementations for
 * {@code equals}, {@code hashCode}, and identifier management.
 * This class is intended to be used within a Jakarta EE environment.
 *
 * @param <ID> the type of the identifier for the entity, which must be serializable
 */
public abstract class AbstractJpaEntity<ID extends Serializable> {

	/**
	 * Gets the identifier for this entity.
	 *
	 * @return the identifier of the entity; may return {@code null} if the entity has not been persisted yet.
	 */
	public abstract ID getId();

	/**
	 * Sets the identifier for this entity.
	 *
	 * @param id the identifier to set.
	 */
	public abstract void setId(ID id);

	@Override
	public boolean equals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null || !obj.getClass().equals(this.getClass())) {
			return false;
		}
		return Objects.equals(this.getId(), ((AbstractJpaEntity<?>) obj).getId());
	}

	@Override
	public int hashCode() {
		final ID id = this.getId();
		return id == null ? 0 : id.hashCode();
	}
}
